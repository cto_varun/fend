import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/pairwise';
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

    constructor(private router: Router) {
        this.router.events.pairwise().subscribe((e) => {
            console.log(e);
        
    });
    }

  ngOnInit() {
  }

}
