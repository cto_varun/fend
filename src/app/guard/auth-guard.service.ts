import { Injectable } from '@angular/core';
import { Router,CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';
@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private route:Router) { }

  canActivate(){
    if(Cookie.get('id') && Cookie.get('token')) {
      console.log("Working");
 
      // this.route.navigateByUrl('/dashboard/(mainpage:mainpage)');
      return true;
    }
    else
    {
      // alert("Kindly, log in first to continue");
      this.route.navigateByUrl('/login');
    }
  }
  
  canActivateChild(rt: ActivatedRouteSnapshot, state: RouterStateSnapshot): any{
    if(Cookie.get('is_admin') == 'false'){
      if(state.url.includes('usermainpage') || state.url.includes('payout') || state.url.includes('showtasks')){
        return true;
      }else
      {
        
        return this.route.navigateByUrl('/404');
      }
    }
    if(Cookie.get('id') && Cookie.get('token')) {
      console.log("child Working");
      console.log(Cookie.get('id'));
      console.log( Cookie.get('token'));  
      return true;
    }
    else
    {
      // alert("Kindly, log in first to continue");
      console.log('login');
      this.route.navigateByUrl('/login');
      return false;
    }
  }
}
