import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';

@Injectable()
export class RulesService {

  private _url = "http://localhost:3000/v1/";
  public user_id ;
  public token ;

  constructor (private _http : Http , private commonService : CommonService){
    this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
  }

  getRules(){
    let api_url=this.commonService.getApiUrl("rules");
    var headers = new Headers();
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.get(api_url,{ headers : headers}) 
    .map(res => res.json());
  }
  

  getRulesByPlan(plan_id){
   let api_url=this.commonService.getApiUrl("findRuleByPlan");
    var headers = new Headers();
    var dt = {
            "plan_id":plan_id
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,dt,{ headers : headers}) 
    .map(res => res.json()); 
  }

  updateRule(id,amount){
   let api_url=this.commonService.getApiUrl("updateRule");
    var headers = new Headers();
    var dt = {
            "id":id,
            "amount" : amount
        };
    headers.append('Content-Type','application/json');
    headers.append('User-Id',this.user_id);
    headers.append('Token',this.token);
    return this._http.post(api_url,dt,{ headers : headers}) 
    .map(res => res.json());  
  }

}
