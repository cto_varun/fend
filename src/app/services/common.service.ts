import { Injectable } from '@angular/core';
import{ MdSnackBar } from '@angular/material'; 
import { Router } from '@angular/router';
import { LoadingAnimateService } from 'ng2-loading-animate';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable()
export class CommonService {

  constructor(private mdsnackbar : MdSnackBar, private router: Router,private _http : Http,private _loadingSvc: LoadingAnimateService,
    private _flashMessagesService: FlashMessagesService) { }

  showSnackBar(msg){
  	return  this.mdsnackbar.open(msg, 'close',{
  		duration:10000
  	});
  }

   showMessage(msg,type){
    if(type == "success" )
    {
      this._flashMessagesService.grayOut(true);
     return  this._flashMessagesService.show(msg, { cssClass: 'alert-successs', timeout: 2000 });
    }
    else
    {
      this._flashMessagesService.grayOut(true);
     return this._flashMessagesService.show(msg, { cssClass: 'alert-dangers  ', timeout: 4000 });
   }
  }

   nav(route_name){
     
    this.router.navigateByUrl("/dashboard/(" + route_name + ":" + route_name +")" ) ;
  }

  getApiUrl(controllerName){
    let url = this.getCableUrl('no') + "v1/" + controllerName;
  	return url;
  }

  getCableUrl(cableName){
  	let url =  "http://localhost:3000/"
  	// let url =  "http://api.inspirovation.com/"
    if(cableName != "no")
  	  return url + cableName;
    else
      return url;
  }

    resetPassword(email){

      let api_url=this.getApiUrl("forgotPassword");
    var headers = new Headers();
    headers.append('Content-Type','application/json');
     let data = {
                   "email" : email
                }
    return this._http.post(api_url,data,{ headers : headers}) 
    .map(res => res.json());
    }

    updatePassword(password,token){ 
    let api_url=this.getApiUrl("updatePassword");
    var headers = new Headers();
    headers.append('Content-Type','application/json');
     let data = {
         "password" : password,
         "token":token
     }
    return this._http.post(api_url,data,{ headers : headers}) 
    .map(res => res.json());
    }
}
