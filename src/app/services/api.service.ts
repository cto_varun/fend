
import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {CommonService} from './common.service';
@Injectable()
export class ApiService {

  public user_id ;
  public token ;	

	constructor (private _http : Http,private commonService : CommonService){
   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
	}
    
    // new user or signup
	postUserData(email,password){
		let user = JSON.stringify({"user":
				{
					"email" : email,
					"password" : password
				}}); 
		let p_url = this.commonService.getApiUrl("users");
		var headers = new Headers();
		headers.append('Content-Type','application/json');
		return this._http.post(p_url,user,{ headers : headers}) 
		.map(res => res.json())
	}

	// authenticate or login user
	authenticateUser(email,password){
		let user = JSON.stringify({"user":
				{
					"email" : email,
					"password" : password
				}});
		let p_url = this.commonService.getApiUrl("authenticate");
		var headers = new Headers();
		headers.append('Content-Type','application/json');
		return this._http.post(p_url,user,{ headers : headers})
		.map(res => res.json())
		}

	// logout user
	logout(user_id,token){
		let user = JSON.stringify(
				{
					"user_id" : user_id,
					"token" : token
				}); 
		let p_url = this.commonService.getApiUrl("logout");
		var headers = new Headers();
		headers.append('User-Id',this.user_id);
    	headers.append('Token',this.token);
		headers.append('Content-Type','application/json');
		return this._http.post(p_url,user,{ headers : headers}) 
		.map(res => res.json())
	}

	// check uniqueness of email
	checkEmail(email){
		let user_email = JSON.stringify({
			"email" : email
		});
		let p_url = this.commonService.getApiUrl("checkEmail");
		var headers = new Headers();
		headers.append('Content-Type','application/json');
		return this._http.post(p_url,user_email,{ headers : headers})
		.map(res => res.json());
	}
}
