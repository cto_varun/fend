import { Injectable } from '@angular/core';
import {Http,Response} from '@angular/http';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {CommonService} from  './common.service';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Injectable()
export class DutyHourService {
  private _apiurl = "http://localhost:3000/v1/";
  public plan_data;
  private user_id; 
  private token ;
  
  constructor(private _http : Http,private _loadingSvc: LoadingAnimateService,
    private commonService : CommonService) 
  { 
   this.user_id = Cookie.get('id');
   this.token = Cookie.get('token');
  }


  fetchDutyHours(){
    let api_url=this.commonService.getApiUrl("dutyworkinghours");
    return this._http.get(api_url) 
    .map(res => res.json());
  }

  	submitDutyHours(id,from,to,relaxation)
    {

  	console.log(from);
  	console.log(to);
    console.log(relaxation);
    let api_url=this.commonService.getApiUrl("updateDutyHours");
    var headers = new Headers();
    var duty = {
            "id" : id,
            "from" : from,
            "to" : to,
            "relaxation" : relaxation
        };
    // headers.append('User-Id',this.user_id);
    // headers.append('Token',this.token);
    headers.append('Content-Type','application/json');
    return this._http.post(api_url,duty,{ headers : headers}) 
    .map(res => res.json());
  	

  }
}
