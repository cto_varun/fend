import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {PayoutReportingService} from '../../../services/payout-reporting.service';
@Component({
  selector: 'app-payout-report',
  templateUrl: './payout-report.component.html',
  styleUrls: ['./payout-report.component.css']
})
export class PayoutReportComponent implements OnInit {

  public job_id:any;
	public payout:any;
  constructor( private payoutReportingService : PayoutReportingService, private route: ActivatedRoute) { }

  ngOnInit() {
  	this.job_id = this.route.snapshot.params['id'];
  	this.getPayoutReporting();
  }

  getPayoutReporting(){
  	this.payoutReportingService.getPayoutReporting(this.job_id)
  		.subscribe(data=>{console.log(data);
  					this.payout = data;},
  			    error=>console.log(error),
  			    ()=>console.log("function called after calculating payout for agent"));
  }

}
