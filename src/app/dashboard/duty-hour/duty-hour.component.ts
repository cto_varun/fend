import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
import {DutyHourService} from '../../services/duty-hour.service';
import { Router  } from '@angular/router';
import {CommonService} from '../../services/common.service';
@Component({
  selector: 'app-duty-hour',
  templateUrl: './duty-hour.component.html',
  styleUrls: ['./duty-hour.component.css']
})
export class DutyHourComponent implements OnInit {
	dutyHours: FormGroup;
  flag = 0;
  constructor(private _fb:FormBuilder,private dutyHour : DutyHourService,private _router : Router,private commonService : CommonService) { }

  ngOnInit() {
    

  	this.dutyHours = this._fb.group({
      'id':[null],
  	  'to' : [null,Validators.compose([Validators.required])],
  	  'from' : [null,Validators.compose([Validators.required])],
      'relaxation' : [null]
  	})
    this.commonService.showMessage("Fetching Detail..Please Wait!" , "success");
        this.dutyHour.fetchDutyHours()
          .subscribe(
            data => {

              console.log(data);
              const duty_hours = {
              id: data.id,
              from : data.from,
              to : data.to,
              relaxation:data.relaxation
              };
             this.dutyHours.patchValue(duty_hours);
            },
            error => {console.log(error);
            this.commonService.showMessage("Something went wrong..Please Try Again!" , "danger");
            },
            ()=> console.log("function called after fetching gps range"));


  }

  // setting duty hours by admin
  submitDutyHour(){
    this.flag = 1;
    this.commonService.showMessage("Submitting Duty Hours detail" , "success");
  	this.dutyHour.submitDutyHours(this.dutyHours.value.id,this.dutyHours.value.from,this.dutyHours.value.to,this.dutyHours.value.relaxation)
  		.subscribe(
  			data => {
          this.flag = 0;
          console.log(data);
          this.commonService.showSnackBar("Duty Working Hours and Relaxation Hours Updated Succesfully");
          },
  			error=> {console.log(error);
                this.flag = 0;                
                this.commonService.showSnackBar(" Error Occured While Updating Duty Working Hours and Relaxation Hours ");
        },
  			()=>{console.log("function called after submitting duty hours")});
  }
}
