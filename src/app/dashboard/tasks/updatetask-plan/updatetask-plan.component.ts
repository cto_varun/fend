import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import { TasksService } from '../../../services/tasks.service';
import {AgentsService} from '../../../services/agents.service';
import {FormBuilder,FormGroup,NgForm,Validators} from '@angular/forms';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-updatetask-plan',
  templateUrl: './updatetask-plan.component.html',
  styleUrls: ['./updatetask-plan.component.css']
})
export class UpdatetaskPlanComponent implements OnInit {

  public task_id;
  public fleet_id;
  public plan_assign;
  public agent;
  public error_msg;
  constructor(private router : Router,
  	private activatedRoute: ActivatedRoute,
  	private taskService: TasksService,
  	private agentsService: AgentsService,
    private commonService:CommonService) { }

  ngOnInit() {
  	this.task_id = this.activatedRoute.snapshot.params['taskid'];
  	this.fleet_id = this.activatedRoute.snapshot.params['fleetid'];

    // fetching agent from the api
  	this.agentsService.getAgentById(this.fleet_id)
                    .subscribe(
                      data=>{
                        this.agent=data;
                        // fetching task_assigned detail
                        this.taskService.fetchJobId(this.task_id)
                          .subscribe(
                            data => {this.plan_assign = data;
                              console.log(this.plan_assign);},
                            error => console.log(error),
                            () => console.log("assigned plan fetched"));
                    	},
                      error=>{this.error_msg = JSON.parse(error._body); //fetching error message
                         this.error_msg = this.error_msg.msg;
                         this.commonService.showSnackBar(this.error_msg);
                        this.commonService.nav('agent');
                         },
                      ()=>console.log("sucessfully fetched")
                      );
    
  	}

  // updating the plan
  updatePlan(form :NgForm,agent_name){
  	this.taskService.updatePlanAssignedToTask(form.value.task_id,form.value.plan_id)
  		.subscribe(
  			data => {console.log(data);
                  this.commonService.showSnackBar("Plan updated for "+ agent_name+ "  Succesfully.");
                  this.commonService.nav('task');
                },
  			error => {console.log(error)
                  this.commonService.showSnackBar("Task not Assigned to Agent Try Again Later.");
                 },
  			() =>console.log("plan assigned to task updated"));
  }
}
