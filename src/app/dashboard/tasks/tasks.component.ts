import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../services/tasks.service';
import {FormBuilder,FormGroup,NgForm,Validators} from '@angular/forms';
import { Router  } from '@angular/router';
import { LoadingAnimateService } from 'ng2-loading-animate';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

	public tasks: any[] = [];
  public  value;
  public taskdetail;
  public flag;
  public taskdetails=[];
  taskForm: FormGroup;
  public job_id;
  constructor( private taskService: TasksService,
  private _fb:FormBuilder,private router:Router,
  private _loadingSvc: LoadingAnimateService,
  private commonService : CommonService,
) {
   }

  ngOnInit() {
  	this.fetchTasks();
  }

//fetch Tasks
fetchTasks(){
  this.commonService.showMessage("Fetching Task Detail..Please Wait!" , "success");
  this.taskService.syncTasksFromTookan()
  .subscribe(
    data=> {
            this.tasks = data; //assigning the response to the agents variable
            console.log(this.tasks);
             this._loadingSvc.setValue(false);
    },
    error=>{console.log(error);
    this.commonService.showMessage("Something went wrong..Please wait!" , "danger");
    },
    ()=>console.log("function called after fetching tasks"));
}

// fetch task detail from api
taskDetail(address,start_time,finish_time,name,uniqueId){

  this.taskService.fetchTaskDetailFromApi(address,start_time,finish_time,name)
  .subscribe(
    data=>{
      console.log(data),
      this.taskdetails[uniqueId]= data;
      // console.log(this.taskdetails);
    },
    error=>console.log(error),
    ()=>console.log("task detail fetched"));
}

// navigating to different pages
  nav(route_name,task_id,fleet_id){
    this.router.navigateByUrl("/dashboard/(" + route_name + ":" +route_name +"/"+task_id+"/"+fleet_id+")") ;
  }

// assigning task to the agent
submitForm(form : NgForm,address,start_time,finish_time,name,uniqueId,fleet_name){
  console.log(form);
  this.commonService.showMessage("Assiging Task to agent..Please Wait!" , "success");

  this.taskService.addAssignTaskToApi(form.value.job_id,form.value.fleet_id,form.value.plan_id)
      .subscribe(
        data=> {
          this.taskDetail(address,start_time,finish_time,name,uniqueId);
            
            this.commonService.showSnackBar('Task assigned to ' + fleet_name + ' successfully')
        },
        error=>{console.log(error);
         this.commonService.showSnackBar('Error while assiging task to agent ' +fleet_name+'..Please Try Again');
        },
        ()=>{console.log("task assigned to the agent")});
}

}
