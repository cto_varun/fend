import { Component, OnInit } from '@angular/core';
import{CommonService} from '../../../services/common.service';
@Component({
  selector: 'app-dummy-task',
  templateUrl: './dummy-task.component.html',
  styleUrls: ['./dummy-task.component.css']
})
export class DummyTaskComponent implements OnInit {

  constructor( private commonService : CommonService) { }

  ngOnInit() {

  	this.commonService.nav("task");
  }

}
