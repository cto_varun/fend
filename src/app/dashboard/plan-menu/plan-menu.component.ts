import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'app-plan-menu',
  templateUrl: './plan-menu.component.html',
  styleUrls: ['./plan-menu.component.css']
})
export class PlanMenuComponent implements OnInit {

              
  constructor(private router:Router,
    private commonService:CommonService) { }

  ngOnInit() {
  }

}
