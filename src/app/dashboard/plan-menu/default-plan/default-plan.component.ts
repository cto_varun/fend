import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../services/common.service';
import {PlanService} from '../../../services/plan.service';
import {FormBuilder,FormGroup,NgForm,Validators} from '@angular/forms';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Component({
  selector: 'app-default-plan',
  templateUrl: './default-plan.component.html',
  styleUrls: ['./default-plan.component.css']
})
export class DefaultPlanComponent implements OnInit {

	public defaultplans ;

  constructor(private commonService:CommonService,
    		  private planService: PlanService,
    		  private _loadingSvc: LoadingAnimateService,

  			) { }

  ngOnInit() {

  	this.fetchPlans()

  }


fetchPlans(){
    // fetching all plans
    this.planService.fetchDefaultPlansFromApi()
    .subscribe(data=> { this.defaultplans = data;
                         this._loadingSvc.setValue(false);
                      },
      error=>console.log(error),
      ()=>console.log("plans fetched")); 
  }
  // //updating Default plan
  // updateDeafaultPlan(form :NgForm){
  //   this.planService.updateDefaultPlan(form.value.id,form.value.defaultPlan)
  //     .subscribe(
  //       data => {console.log(data);
  //                 this.fetchPlans();
  //                 this.commonService.showSnackBar("Plan Updated Successfuly")
  //               },
  //       error=> console.log(error),
  //       ()=>console.log("plan edited"));
  // }

}
