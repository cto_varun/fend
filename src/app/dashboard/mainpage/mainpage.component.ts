import { Component, OnInit } from '@angular/core';
import {MainpageService} from '../../services/mainpage.service';
import { Router  } from '@angular/router';
import {CommonService} from '../../services/common.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
	public task_count;
	public plan_count;
	public agent_count;
	public new_tasks;
	public new_agents;
  constructor(private mainPageService : MainpageService,
  	private router:Router,
  	private commonService:CommonService,
	private _loadingSvc: LoadingAnimateService ) { }

  ngOnInit() {
this.commonService.showMessage(" Preparing Your Dashboard.. Please Wait","success");
this._loadingSvc.setValue(false);
this.taskCountFetched();
this.planCountFetched();
this.agentCountFetched();
this.newTaskFetched();
this.newAgentFetched();
  }


	taskCountFetched(){
			this.mainPageService.getTaskCount(Cookie.get('id'),Cookie.get('token'))
		.subscribe(
				data=> {this.task_count = data;
								this._loadingSvc.setValue(false);
							 },
				error=>{console.log(error);
				this.commonService.showMessage("Something Went Wrong..Please Try Again Later","danger");
								
							},
				()=>console.log("task count fetched")
			);
	}
	planCountFetched(){
			this.mainPageService.getPlanCount(Cookie.get('id'),Cookie.get('token'))
		.subscribe(
				data=> {this.plan_count = data;
								this._loadingSvc.setValue(false);							
						 },
				error=>{console.log(error);
								this.commonService.showMessage("Something Went Wrong..Please Try Again Later","danger");
							},
				()=>console.log("plan count fetched")
			);  	
	}
	agentCountFetched(){
			this.mainPageService.getAgentCount(Cookie.get('id'),Cookie.get('token'))
		.subscribe(
				data=> {this.agent_count = data;
								this._loadingSvc.setValue(false);							
							 },
				error=>{console.log(error)	;
								this.commonService.showMessage("Something Went Wrong..Please Try Again Later","danger");
							 },
				()=>console.log("agent count fetched")
				  );
	}
	newTaskFetched(){
			this.mainPageService.getNewTasks(Cookie.get('id'),Cookie.get('token'))
		.subscribe(
				data=> {this.new_tasks = data;
								this._loadingSvc.setValue(false);					
							 },
				error=>{console.log(error);
								this.commonService.showMessage("Something Went Wrong..Please Try Again Later","danger");
							 },
				()=>console.log("New tasks fetched")
				  );
	}


	newAgentFetched(){
			this.mainPageService.getNewAgents(Cookie.get('id'),Cookie.get('token'))
		.subscribe(
					data=> {this.new_agents = data;
									this._loadingSvc.setValue(false);					
									console.log(data);
								 },
					error=>{console.log(error);
									this.commonService.showMessage("Something Went Wrong..Please Try Again Later","danger");
								 },
					()=>console.log("New agents fetched")
				  );
	}
}
