import { Component, OnInit } from '@angular/core';
import {PayoutService} from '../../../services/payout.service';
import { TasksService } from '../../../services/tasks.service';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-show-tasks',
  templateUrl: './show-tasks.component.html',
  styleUrls: ['./show-tasks.component.css']
})
export class ShowTasksComponent implements OnInit {
	public tasks;
	public payout;
  constructor(private payoutService : PayoutService,
  	private taskService: TasksService, private router: Router) { }

  ngOnInit() {
  	this.taskService.fetchTaskDetailOfAgent(Cookie.get('id'))
  		.subscribe(
  			data=>{console.log(data);
  				this.tasks = data},
  			error=>console.log(error),
  			()=>console.log("function called after fetching tasks assigned to agent")
  			);
  }



  checkPayout(job_id){
  	// navigating to different pages
  	this.router.navigateByUrl("/dashboard/(payout:payout/"+job_id+")") ;
  	}

}
