import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayoutComponent } from './payout.component';
import { ShowTasksComponent } from './show-tasks/show-tasks.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PayoutComponent, ShowTasksComponent]
})
export class PayoutModule { }
