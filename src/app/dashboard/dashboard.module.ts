import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { AgentsComponent } from './agents/agents.component';
import {DashboardAgentService} from './dashboard-agent.service';
import { PlansComponent } from './plan-menu/plans/plans.component';
import { ShowPlansComponent } from './plan-menu/show-plans/show-plans.component';
import { PlanMenuComponent } from './plan-menu/plan-menu.component';
import { TasksComponent } from './tasks/tasks.component';
import { UpdatetaskPlanComponent } from './tasks/updatetask-plan/updatetask-plan.component';
import { DummyTaskComponent } from './tasks/dummy-task/dummy-task.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DashboardComponent, MainpageComponent,AgentsComponent, PlansComponent, ShowPlansComponent,TasksComponent, EditPlanComponent, PlanMenuComponent, UpdatetaskPlanComponent, DummyTaskComponent],
  providers: [DashboardAgentService]
})
export class DashboardModule { }
