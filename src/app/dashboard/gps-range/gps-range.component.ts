 import { Component, OnInit } from '@angular/core';
 import {FormBuilder,FormGroup,Validators} from '@angular/forms';
 import {GpsRangeService} from '../../services/gps-range.service';
 import { Router  } from '@angular/router';
import {CommonService} from '../../services/common.service'; 

@Component({
  selector: 'app-gps-range',
  templateUrl: './gps-range.component.html',
  styleUrls: ['./gps-range.component.css']
})
export class GpsRangeComponent implements OnInit {
	gpsRange: FormGroup;
  public flag = 0;
  constructor(private _fb:FormBuilder,
    private gpsRanges : GpsRangeService,
    private _router : Router,
    private commonService : CommonService) { }

  ngOnInit() {	
  	  this.gpsRange = this._fb.group({
      'id' : [null,Validators.compose([Validators.required])],
  	  'range' : [null,Validators.compose([Validators.required])]    
  	  })
    this.commonService.showMessage("Fetching Detail..Please Wait!" , "success");

      this.gpsRanges.fetchGpsRange()
          .subscribe(
            data => {
              console.log(data);
              const gps_range = {
              id: data.id,
              range : data.range
              };
             this.gpsRange.patchValue(gps_range);
            },
            error => {console.log(error);
            this.commonService.showMessage("Something went wrong..Please Try Again" , "danger");
            },
            ()=> console.log("function called after fetching gps range"));

  }

  // setting duty hours by admin
  submitGpsRange(){
  this.flag = 1;
    this.commonService.showMessage("Submitting Detail..Please Wait!" , "success");

  	this.gpsRanges.submitGpsRange(this.gpsRange.value.id,this.gpsRange.value.range)
  		.subscribe(
  			data => {
          this.flag = 0;
          console.log(data);
          this.commonService.showSnackBar(" GPS Range Updated Successfully");
          // this._router.navigateByUrl('/dashboard/(showplans:showplans)')
          },
  			error=> {console.log(error);
          this.flag = 0;          
          this.commonService.showSnackBar(" Error While Updating GPS Range");

        },
  			()=>console.log("function called after submitting gps range"));
  }

}
