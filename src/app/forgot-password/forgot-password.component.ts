import { Component, OnInit } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { Router  } from '@angular/router';
import {PlanService} from '../services/plan.service';
import {RulesService} from '../services/rules.service';
import {CommonService} from '../services/common.service';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public flag = 0; 
resetPasswordForm: FormGroup;
  constructor(private _fb:FormBuilder,private _router: Router,
  			 private commonService : CommonService) { }

  ngOnInit() {
  	if(Cookie.get('id') && Cookie.get('token')) {
      this._router.navigateByUrl('/dashboard/(mainpage:mainpage)');

    }
    this.resetPasswordForm = this._fb.group({
      'email' : [null,Validators.compose([Validators.required])]
         
    });

  }

   // Sending Email For reset Password
    resetPassword(){
      this.flag = 1;
  	this.commonService.resetPassword(this.resetPasswordForm.value.email)
  		.subscribe(
  			data => {
          this.flag = 0;
          console.log(data);
          this.commonService.showSnackBar("Reset link has been sent to your email");
          this._router.navigateByUrl('/login');
          },
  			error=> {let body = JSON.parse(error._body);
                  this.commonService.showSnackBar(body.msg);
                   this.flag = 0;
                },
  			()=>console.log("function called after reset password"));
  }

}
